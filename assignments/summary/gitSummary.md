 #### Git Summary


![Git Lab](https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.svg)

### Git ?
* Git is version-control software that makes collaboration with teammates super simple.
* Git lets you easily keep track of every revision you and your team make during the development of your software.


![workflow](https://i2.wp.com/build5nines.com/wp-content/uploads/2018/01/GitHub-Flow.png?fit=900%2C310&ssl=1)

### Important Git Features

* Repository
Also, called as repo. It is the collection of files and folders or code files for which we are using git to track.

* Commit
 -It is like saving your work. Commit to a repo means taking picture/snapshot of the files as they exist at that moment. It only exists on our local machine.

* Push
 -It's work is to sync your commits to gitlab.

* Branch
 -Git repo is like a tree. The trunk of the tree or the main software is called the master branch, the branches of the tree are called branches (these are separate instances of code that is diffrent to main code base).

* Merge
 -When a branch is free of bugs and ready to become part of the primary codebase, it will get merged into the master branch.

* Clone
 -It takes the entire repo and makes an exact copy of it.

* Fork
 -It is nearly like cloning, but it makes an entirely new repo of that code under your name.

#### Git Internals

 ![Git Internals](https://snipcademy.com/img/articles/git-fundamentals/three-stages-01.svg)

 
 ![Git fundamentals](https://unwiredlearning.com/wp-content/uploads/2018/07/git-flow-768x513.png)




**Docker Summary**

![docker terms](https://i.pinimg.com/originals/20/ba/1f/20ba1f25b0a0caf98be325bb51f562ac.jpg)

![docker image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSffYU_7jzoXi7kxtalyUwMTzzw4vk-j3RgsA&usqp=CAU)

## Docker Installation

Installing CE (Community Docker Engine)

$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

// Check if docker is successfully installed in your system
$ sudo docker run hello-world

![command](https://www.devteam.space/wp-content/uploads/2017/03/container_explainer.png)

![basic command](https://image.slidesharecdn.com/3hourstodockerfundamentals-jumpstartyourdockerknowledge-160119215359/95/3-hours-to-docker-fundamentals-jumpstart-your-docker-knowledge-16-638.jpg?cb=1453240760)